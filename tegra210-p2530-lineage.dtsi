// SPDX-License-Identifier: GPL-2.0
#include <dt-bindings/mfd/max77620.h>

#include "tegra210-lineage.dtsi"

/ {
	aliases {
		rtc0 = "/i2c@7000d000/max77620@3c";
	};

	gpu@57000000 {
		vdd-supply = <&vdd_gpu>;
	};

	i2c@7000d000 {
		pmic: pmic@3c {
			compatible = "maxim,max77620";
			reg = <0x3c>;
			interrupts = <GIC_SPI 86 IRQ_TYPE_LEVEL_HIGH>;

			#interrupt-cells = <2>;
			interrupt-controller;

			gpio-controller;
			#gpio-cells = <2>;

			pinctrl-names = "default";
			pinctrl-0 = <&max77620_default>;

			max77620_default: pinmux@0 {
				gpio0 {
					pins = "gpio0";
					function = "gpio";
				};

				gpio1 {
					pins = "gpio1";
					function = "fps-out";
					drive-push-pull = <1>;
					maxim,active-fps-source = <MAX77620_FPS_SRC_0>;
					maxim,active-fps-power-up-slot = <7>;
					maxim,active-fps-power-down-slot = <0>;
				};

				gpio2_3 {
					pins = "gpio2", "gpio3";
					function = "fps-out";
					drive-open-drain = <1>;
					maxim,active-fps-source = <MAX77620_FPS_SRC_0>;
				};

				gpio4 {
					pins = "gpio4";
					function = "32k-out1";
				};

				gpio5_6_7 {
					pins = "gpio5", "gpio6", "gpio7";
					function = "gpio";
					drive-push-pull = <1>;
				};
			};

			hog-0 {
				gpio-hog;
				output-high;
				gpios = <2 GPIO_ACTIVE_HIGH>,
					<7 GPIO_ACTIVE_HIGH>;
			};

			fps {
				#address-cells = <1>;
				#size-cells = <0>;

				fps0 {
					reg = <0>;
					maxim,shutdown-fps-time-period-us = <5120>;
					maxim,fps-event-source = <MAX77620_FPS_EVENT_SRC_EN0>;
				};

				fps1 {
					reg = <1>;
					maxim,shutdown-fps-time-period-us = <5120>;
					maxim,fps-event-source = <MAX77620_FPS_EVENT_SRC_EN1>;
					maxim,device-state-on-disabled-event = <MAX77620_FPS_INACTIVE_STATE_SLEEP>;
				};

				fps2 {
					reg = <2>;
					maxim,fps-event-source = <MAX77620_FPS_EVENT_SRC_EN0>;
				};
			};

			regulators {
				in-ldo0-1-supply = <&max77620_sd2>;
				in-ldo7-8-supply = <&max77620_sd2>;

				max77620_sd0: sd0 {
					regulator-name = "vdd-core";
					regulator-enable-ramp-delay = <146>;
					regulator-min-microvolt = <600000>;
					regulator-max-microvolt = <1400000>;
					regulator-ramp-delay = <9100>;
					regulator-always-on;
					regulator-boot-on;

					maxim,active-fps-power-up-slot = <0>;
					maxim,active-fps-source = <MAX77620_FPS_SRC_1>;
				};

				max77620_sd1: sd1 {
					regulator-name = "vddio-ddr";
					regulator-enable-ramp-delay = <130>;
					regulator-ramp-delay = <9100>;
					regulator-always-on;
					regulator-boot-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_0>;
				};

				max77620_sd2: sd2 {
					regulator-name = "vdd-pre-reg";
					regulator-enable-ramp-delay = <176>;
					regulator-min-microvolt = <1350000>;
					regulator-max-microvolt = <1350000>;
					regulator-ramp-delay = <7900>;
					regulator-always-on;
					regulator-boot-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_NONE>;
				};

				max77620_sd3: sd3 {
					regulator-name = "vdd-1v8";
					regulator-enable-ramp-delay = <242>;
					regulator-min-microvolt = <1800000>;
					regulator-max-microvolt = <1800000>;
					regulator-ramp-delay = <7700>;
					regulator-always-on;
					regulator-boot-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_0>;
				};

				max77620_ldo0: ldo0 {
					regulator-name = "avdd-sys";
					regulator-enable-ramp-delay = <26>;
					regulator-min-microvolt = <1200000>;
					regulator-max-microvolt = <1200000>;
					regulator-ramp-delay = <50000>;
					regulator-boot-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_NONE>;
				};

				max77620_ldo1: ldo1 {
					regulator-name = "vdd-pex";
					regulator-enable-ramp-delay = <22>;
					regulator-min-microvolt = <1075000>;
					regulator-max-microvolt = <1075000>;
					regulator-ramp-delay = <50000>;
					regulator-always-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_1>;
				};

				max77620_ldo2: ldo2 {
					regulator-name = "vddio-sdmmc3";
					regulator-enable-ramp-delay = <62>;
					regulator-min-microvolt = <1800000>;
					regulator-max-microvolt = <3300000>;
					regulator-ramp-delay = <50000>;

					maxim,active-fps-source = <MAX77620_FPS_SRC_NONE>;
				};

				max77620_ldo3: ldo3 {
					regulator-name = "vdd-cam-hv";
					regulator-enable-ramp-delay = <50>;
					regulator-min-microvolt = <2800000>;
					regulator-max-microvolt = <2800000>;
					regulator-ramp-delay = <50000>;

					maxim,active-fps-source = <MAX77620_FPS_SRC_NONE>;
				};

				max77620_ldo4: ldo4 {
					regulator-name = "vdd-rtc";
					regulator-enable-ramp-delay = <22>;
					regulator-min-microvolt = <850000>;
					regulator-max-microvolt = <850000>;
					regulator-ramp-delay = <50000>;
					regulator-always-on;
					regulator-boot-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_0>;
				};

				max77620_ldo5: ldo5 {
					regulator-name = "avdd-ts-hv";
					regulator-enable-ramp-delay = <62>;
					regulator-min-microvolt = <3300000>;
					regulator-max-microvolt = <3300000>;
					regulator-ramp-delay = <50000>;

					maxim,active-fps-source = <MAX77620_FPS_SRC_NONE>;
				};

				max77620_ldo6: ldo6 {
					regulator-name = "vdd-ts";
					regulator-enable-ramp-delay = <36>;
					regulator-min-microvolt = <1800000>;
					regulator-max-microvolt = <1800000>;
					regulator-ramp-delay = <50000>;
					regulator-boot-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_NONE>;
				};

				max77620_ldo7: ldo7 {
					regulator-name = "vdd-gen-pll-edp";
					regulator-enable-ramp-delay = <24>;
					regulator-min-microvolt = <1050000>;
					regulator-max-microvolt = <1050000>;
					regulator-ramp-delay = <50000>;
					regulator-always-on;
					regulator-boot-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_1>;
				};

				max77620_ldo8: ldo8 {
					regulator-name = "vdd-hdmi-dp";
					regulator-enable-ramp-delay = <22>;
					regulator-min-microvolt = <1050000>;
					regulator-max-microvolt = <1050000>;
					regulator-ramp-delay = <50000>;
					regulator-always-on;
					regulator-boot-on;

					maxim,active-fps-source = <MAX77620_FPS_SRC_1>;
				};
			};
		};
	};

	battery_reg: regulator-vdd-ac-bat {
		compatible = "regulator-fixed";
		regulator-name = "vdd-ac-bat";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
	};

	vdd_3v3: regulator-vdd-3v3 {
		compatible = "regulator-fixed";
		regulator-name = "vdd-3v3";
		regulator-enable-ramp-delay = <160>;
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;

		gpio = <&pmic 3 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	max77620_gpio7: regulator-max77620-gpio7 {
		compatible = "regulator-fixed";
		regulator-name = "max77620-gpio7";
		regulator-enable-ramp-delay = <240>;
		regulator-min-microvolt = <1200000>;
		regulator-max-microvolt = <1200000>;
		vin-supply = <&max77620_ldo0>;
		regulator-always-on;
		regulator-boot-on;

		gpio = <&pmic 7 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	lcd_bl_en: regulator-lcd-bl-en {
		compatible = "regulator-fixed";
		regulator-name = "lcd-bl-en";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		regulator-boot-on;

		gpio = <&gpio TEGRA_GPIO(V, 1) GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	en_vdd_sd: regulator-vdd-sd {
		compatible = "regulator-fixed";
		regulator-name = "en-vdd-sd";
		regulator-enable-ramp-delay = <472>;
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		vin-supply = <&vdd_3v3>;

		gpio = <&gpio TEGRA_GPIO(Z, 4) GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	en_vdd_cam: regulator-vdd-cam {
		compatible = "regulator-fixed";
		regulator-name = "en-vdd-cam";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;

		gpio = <&gpio TEGRA_GPIO(S, 4) GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	vdd_sys_boost: regulator-vdd-sys-boost {
		compatible = "regulator-fixed";
		regulator-name = "vdd-sys-boost";
		regulator-enable-ramp-delay = <3090>;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;

		gpio = <&pmic 1 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	vdd_hdmi: regulator-vdd-hdmi {
		compatible = "regulator-fixed";
		regulator-name = "vdd-hdmi";
		regulator-enable-ramp-delay = <468>;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		vin-supply = <&vdd_sys_boost>;
		regulator-boot-on;

		gpio = <&gpio TEGRA_GPIO(CC, 7) GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	en_vdd_cpu_fixed: regulator-vdd-cpu-fixed {
		compatible = "regulator-fixed";
		regulator-name = "vdd-cpu-fixed";
		regulator-min-microvolt = <1000000>;
		regulator-max-microvolt = <1000000>;
	};

	vdd_aux_3v3: regulator-vdd-aux-3v3 {
		compatible = "regulator-fixed";
		regulator-name = "aux-3v3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	vdd_snsr_pm: regulator-vdd-snsr-pm {
		compatible = "regulator-fixed";
		regulator-name = "snsr_pm";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;

		enable-active-high;
	};

	vdd_usb_5v0: regulator-vdd-usb-5v0 {
		compatible = "regulator-fixed";
		status = "disabled";
		regulator-name = "vdd-usb-5v0";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		vin-supply = <&vdd_3v3>;

		enable-active-high;
	};

	vdd_cdc_1v2_aud: regulator-vdd-cdc-1v2-aud {
		compatible = "regulator-fixed";
		status = "disabled";
		regulator-name = "vdd_cdc_1v2_aud";
		regulator-min-microvolt = <1200000>;
		regulator-max-microvolt = <1200000>;
		startup-delay-us = <250000>;

		enable-active-high;
	};

	vdd_disp_3v0: regulator-vdd-disp-3v0 {
		compatible = "regulator-fixed";
		regulator-name = "vdd-disp-3v0";
		regulator-enable-ramp-delay = <232>;
		regulator-min-microvolt = <3000000>;
		regulator-max-microvolt = <3000000>;
		regulator-always-on;

		gpio = <&gpio TEGRA_GPIO(I, 3) GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	vdd_fan: regulator-vdd-fan {
		compatible = "regulator-fixed";
		regulator-name = "vdd-fan";
		regulator-enable-ramp-delay = <284>;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;

		gpio = <&gpio TEGRA_GPIO(E, 4) GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	vdd_gpu: regulator-vdd-gpu {
		compatible = "pwm-regulator";
		pwms = <&pwm 1 4880>;
		regulator-name = "vdd-gpu";
		regulator-min-microvolt = <710000>;
		regulator-max-microvolt = <1320000>;
		enable-gpios = <&pmic 6 GPIO_ACTIVE_HIGH>;
		regulator-ramp-delay = <80>;
		regulator-enable-ramp-delay = <1000>;
	};
};
